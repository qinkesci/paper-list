**Paper List（Partial）**

**Updated on 2025.02.09**

1. Dong Y, **Qin K**, Ke P, et al. Cross-graph Knowledge Exchange for Personalized Response Generation in Dialogue Systems[J]. IEEE Internet of Things Journal, 2025.

1. Zakari R Y, Owusu J W, **Qin K**, et al. Seeing and Reasoning: A Simple Deep Learning Approach to Visual Question Answering[J]. Big Data Mining and Analytics, 2025, 8(2): 458-478.

1. Zakari R Y, Owusu J W, **Qin K**, et al. VQA and visual reasoning: An overview of approaches, datasets, and future direction[J]. Neurocomputing, 2025: 129345.

1. Qiu J, Zhu T, **Qin K**, et al. The interaction network and potential clinical effectiveness of dimensional psychopathology phenotyping based on EMR: a Bayesian network approach[J]. BMC psychiatry, 2025, 25(1): 81.

1. Dong Y, **Qin K**, Liang S, et al. GKA-GPT: Graphical knowledge aggregation for multiturn dialog generation[J]. Knowledge-Based Systems, 2025, 309: 112763. 

1. Yuezhou Dong, **Ke Qin**, Shuang Liang, Hierarchical Knowledge Aggregation for Personalized Response Generation in Dialogue Systems, CCF International Conference on Natural Language Processing and Chinese Computing (NLPCC2024). 2024, 29-42

1. Li Muquan, Dongyang Zhang, Tao He, Xiurui Xie, Yuan-Fang Li, and **Ke Qin**. "Towards Effective Data-Free Knowledge Distillation via Diverse Diffusion Augmentation." In Proceedings of the 32nd ACM International Conference on Multimedia (ACM MM2024), pp. 4416-4425. 2024.

1. Miao, Li-Teng, Yi-Zhuo Ma, **Ke Qin**, Rui-Ting Dai, and Ahmad Raza. "ACT-R Theory Can Promote Personality Analysis of Social Network Subjects." In International Conference on Intelligent Computing (ICIC2024), pp. 15-26. Singapore: Springer Nature Singapore, 2024.

1. Dai, L. and **Qin, K.**, 2024, June. ToFC: Tree-of-Fact with Continued Best-First Search for Commonsense Reasoning. In 2024 International Joint Conference on Neural Networks (IJCNN2024) (pp. 1-8). IEEE.

1. Ma, Y., **Qin, K.** and Liang, S., 2024, June. Beta-LR: Interpretable Logical Reasoning based on Beta Distribution. In Findings of the Association for Computational Linguistics: NAACL 2024 (pp. 1945-1955).

1. Wang H, Zhang D, Liu G, Huang L, **Qin K.** Enhancing relation extraction using multi-task learning with SDP evidence. Information Sciences. 2024, 670:120610.

1. Zhang D, Liang S, He T, Shao J, **Qin Ke**. CVIformer: Cross-View Interactive Transformer for Efficient Stereoscopic Image Super-Resolution[J]. **IEEE Transactions on Emerging Topics in Computational Intelligence**, 2024.

1. Hailin Wang, Dan Zhang, Guisong Liu, Li Huang, **Ke Qin**， Enhancing relation extraction using multi-task learning with SDP evidence， **Information Sciences**， 670, 2024, 120610. 

1. Liu C, Zhang D, **Qin K.** Knowledge Distillation for Single Image Super-Resolution via Contrastive Learning. InProceedings of the 2024 International Conference on Multimedia Retrieval (ICMR2024), 2024 May 30 (pp. 1079-1083).

1. Dai L, **Qin K**. ToFC: Tree-of-Fact with Continued Best-First Search for Commonsense Reasoning[C], 2024 International Joint Conference on Neural Networks (IJCNN2024). 2024: 1-8.

1. Zeng X, Bai Z, **Qin K**, et al. EiGC: An Event-Induced Graph with Constraints for Event Causality Identification[J]. **Electronics**, 2024, 13(23): 4608.

1. Qiu J, Yu C, Kuang Y, Zhu T, **Qin K**, Zhang W. Association between psychiatric symptoms with multiple peripheral blood sample test: a 10-year retrospective study[J]. Frontiers in Psychiatry, 2024, 15: 1481006.

1. Qiuyi Qi, Tuo Shi,  **Ke Qin**  and Guangchun Luo, Completion Time Optimization in UAV-Relaying-Assisted MEC Networks with Moving Users, **IEEE Transactions on Consumer Electronics**, doi: 10.1109/TCE.2023.3278470, 2023.

1. Wang H,  **Qin K** , Duan G, et al. Denoising Graph Inference Network for Document-Level Relation Extraction[J]. Big Data Mining and Analytics, 2023, 6(2): 248-262.

1. Wang H, **Qin K**, Lu G, et al. Deep neural network-based relation extraction: an overview[J]. **Neural Computing and Applications**, 2022: 1-21.

1. Wang H, **Qin K**, Lu G, et al. Document-level relation extraction using evidence reasoning on RST-GRAPH[J]. **Knowledge-Based Systems**, 2021: 107274.[link](https://www.sciencedirect.com/science/article/pii/S0950705121005360)

1. Yin J, Wang J, Jiang J, Sun Y, Chen X, **Qin Ke**. Research on the Construction and Application of Breast Cancer-specific Database System Based on Full Data Lifecycle[J]. **Frontiers in Public Health**, 2021, 9: 936. [link](https://www.frontiersin.org/articles/10.3389/fpubh.2021.712827/full?&utm_source=Email_to_authors_&utm_medium=Email&utm_content=T1_11.5e1_author&utm_campaign=Email_publication&field=&journalName=Frontiers_in_Public_Health&id=712827)

1. Duan G, Yang H,  **Qin K** , et al. Improving Neural Machine Translation Model with Deep Encoding Information[J]. Cognitive Computation, 2021: 1-9. [link](https://link.springer.com/article/10.1007/s12559-021-09860-7)

1. Min S, Gao Z, Peng J, **Qin K** et al. STGSN-A Spatial-Temporal Graph Neural Network framework for time-evolving social networks[J]. **Knowledge-Based Systems**, 106746.[Link](https://www.sciencedirect.com/science/article/pii/S0950705121000095?utm_campaign=STMJ_AUTH_SERV_PUBLISHED&utm_medium=email&utm_acid=75710452&SIS_ID=&dgcid=STMJ_AUTH_SERV_PUBLISHED&CMX_ID=&utm_in=DM113290&utm_source=AC_)

1. Ainam J P, **Qin K**, Owusu J W, Lu Guoming. Unsupervised domain adaptation for person re-identification with iterative soft clustering[J]. **Knowledge-Based Systems**, 2020: 106644. [Link](http://www.sciencedirect.com/science/article/abs/pii/S0950705120307735)
 
1. Zhongyang Xiong, **Ke Qin\***, Haobo Yang, Guangchun Luo. Learning Chinese Word Representation Better By Cascade Morphological N-gram, **Neural Computing and Applications**. 2020:1-12. [Link](https://link.springer.com/article/10.1007/s00521-020-05198-7). 
 
1. Hailin Wang, **Ke Qin\***, Guoming Lu, Guangchun Luo, Guisong Liu. Direction-sensitive relation extraction using Bi-SDP attention model. **Knowledge-Based Systems,** 2020, 198, 105928, 1-13. [Link](https://www.sciencedirect.com/science/article/pii/S0950705120302628?via%3Dihub) 
 
1. Jean-Paul Ainam，**Ke Qin\*,** Guisong Liu, Guangchun Luo, Brighter Agyemang. Enforcing Affinity Feature Learning through Self-attention for Person Re-identification, **ACM Transactions on Multimedia Computing, Communications, and Applications**, 2020, 16(1). [Link](https://dl.acm.org/doi/10.1145/3377352) 
 
1. **Ke Qin.** On Chaotic Neural Network Design — A New Framework. **Neural Processing Letters**, 45(1):243-261, 2017.02. [link](http://link.springer.com/article/10.1007/s11063-016-9525-y) 
 
1. Guangchun Luo, Haifeng Sun, **Ke Qin**, Junbao Zhang. Greedy Zone Epidemic Routing in Urban VANETs. **IEICE Transactions on Communications**, 2015, E98-B(01): 219-230. 
 
1. **K. Qin,** B. J. Oommen. Logistic neural networks: Their chaotic and pattern recognition properties. **Neurocomputing**, 125:184–194, 2014.02.[ link](http://www.sciencedirect.com/science/article/pii/S0925231213005109) 
 
1. Y. C. Shi, P. Y. Zhu, **Ke Qin.** Projective synchronization of different chaotic neural networks with mixed time delays based on an integral sliding mode controller. **Neurucomputing**, 123:443–449, 2014. 
 
1. Y. Ma, S. Z. Zhu, **Ke Qin**. Combining the requirement information for software defect estimation in design time. **Information Processing Letters**, 114(9): 469-474, 2014. 
 
1. Guangchun Luo, Junbao Zhang, Haojun Huang, **Ke Qin**, and Haifeng Sun. Exploiting Inter-contact Time for Routing in Delay Tolerant Networks. **Transactions on Emerging Telecommunications Technologies**, 2013, 24(6): 589-599. 
 
1. G. C. Luo, J. S. Ren, **K. Qin\***. Dynamical associative memory: The properties of the new weighted chaotic adachi neural network. **IEICE Transactions on Information and Systems**, E95d(8):2158–2162, 2012. [ link](https://www.jstage.jst.go.jp/article/transinf/E95.D/8/E95.D_2158/_article)  
 
1. Guangchun Luo, Ying Ma, **Ke Qin**. Active Learning for Software Defect Prediction. **IEICE Transactions on Information & Systems**, 2012, E95-D(6):1680-1683.
 
1. Guangchun Luo, Junbao Zhang, **Ke Qin**, and Haifeng Sun. Location-Aware Social Routing in Delay Tolerant Networks. **IEICE Transactions on Communications**. 2012, E95-B(5), 1826-1829. 
 
1. Guangchun Luo, Ying Ma, **Ke Qin**. Asymmetric Learning Based on Kernel Partial Least Squares for Software Defect Prediction. **IEICE Transactions on Information and Systems**, 2012, E95-D(7):2006-2008.  
 
1. **K. Qin,** B. J. Oommen. Adachi-like chaotic neural networks requiring linear-time computations by enforcing a tree-shaped topology. **IEEE Transactions on Neural Networks**, 20(11):1797–1809, 2009.[ link](http://ieeexplore.ieee.org/abstract/document/5272368/)
 
1. Y. Ma, **Ke Qin**, S. Z. Zhu. Discrimination Analysis for Predicting Defect-Prone Software Modules. **Journal of Applied Mathematics**. http://dx.doi.org/10.1155/2014/675368, 2014.
 
1. Ningduo Peng, Guangchun Luo, **Ke Qin**, Aiguo Chen. Query-Biased Preview over Outsourced and Encrypted Data. **The Scientific World Journal**, 2013, http://dx.doi.org/10.1155/2013/860621. 
 
1. Guangchun Luo, Ningduo Peng, **Ke Qin**, Aiguo Chen. A Layered Searchable Encryption Scheme with Functional Components Independent of Encryption Methods. **The Scientific World Journal,** 2014, http://dx.doi.org/10.1155/20- 14/153791.
 
1. Ying Ma, **Ke Qin**, Shunzhi Zhu. Discrimination Analysis for Predicting Defect-Prone Software Modules. **Journal of Applied Mathematics**, 2014, http://dx.doi.org/10.1155/2014/675368. 
 
1. **K. Qin,** B. J. Oommen. Chaotic Neural Networks with a Random Topology Can Achieve Pattern Recognition. **Chaotic Modeling and Simulation**, 4:583-590, 2013[ link](http://www.cmsim.eu/papers_pdf/october_2013_papers/10_CMSIM-Journal_2013_Qin_Oommen_4_583-590.pdf)
 
1. **K. Qin,** B. J. Oommen. Ideal chaotic pattern recognition is achievable: The ideal-m-adnn – its design and properties. **Transactions on Computational Collective Intelligence XI**, 8065:22–51, 2013.[ link](http://link.springer.com/chapter/10.1007/978-3-642-41776-4_2)
 
1. **K. Qin**, B. J. Oommen. The entire range of chaotic pattern recognition properties possessed by the Adachi neural network. **Intelligent Decision Technologies**, 6(1):27–41, 2012.[ link](http://content.iospress.com/articles/intelligent-decision-technologies/idt00120)
 
1. **K. Qin,** B. J. Oommen. Ideal chaotic pattern recognition using the modified Adachi neural network. **Chaotic Modeling and Simulation**, 4:701–710, 2012. [link](https://pdfs.semanticscholar.org/73a5/29169074ea7b6c6ac8bcc90801c7095e33d0.pdf)
 
1. **K. Qin,** B. J. Oommen. An enhanced tree-shaped Adachi-like chaotic neural network requiring linear-time computations. **Chaotic Systems: Theory and Applications**, 284–293, 2010.[ link](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10..613.73&rep=rep1&type=pdf)
 
1. **K. Qin,** B. J. Oommen. Chaotic Neural Networks with a “Small-World” Topology Can Achieve Pattern Recognition, **Chaotic Modeling and Simulation**, 4:379–386, 2014. [link](http://www.cmsim.eu/papers_pdf/october_2014_papers/7_CMSIM-Journal_2014_Qin_Oommen_4_379-386.pdf)
 
1. J. S. Ren, K. Qin*, G. C. Luo. On Software Defect Prediction Using Machine Learning. **Journal of Applied Mathematics**. http://dx.doi.org/10.1155/2014/785435, 2014. [link](https://www.hindawi.com/journals/jam/2014/785435/abs/)

1. Jean-Paul Ainam, **K. Qin\***, Guisong Liu, Guangchun Luo. Person Re-identification through Clustering and Partial Label Smoothing Regularization. In proceedings of *the 2nd International Conference on Software Engineering and Information Management (ICSIM'19)*, 189-193, January 10–13, 2019, Bali, Indonesia. ACM, New York, USA.[ link](file:///H:/%E9%98%BF%E9%87%8C%E4%BA%91/%20https:/dl.acm.org/citation.cfm?id=3305205)

1. Jean-Paul Ainam, **K. Qin\***, Guisong Liu, Guangchun Luo. Deep Residual Network with Self Attention Improves Person Re-Identification Accuracy. In proceedings of *the 2019 11th International Conference on Machine Learning and Computing (ICMLC'19)*, 380-385, February 22–24, 2019, Zhuhai, China. ACM, New York, USA. [link](https://dl.acm.org/citation.cfm?id=3318324)

1. Haobo Yang, Zongyang Xiong, Jiexin Zhang, **Ke Qin\***, Guoming Lu, Cascade Morphological n-gram can Improve Chinese Words Representation Learning. In proceedings of *the 2019 IEEE Green Computing and Communications (GreenCom'19)*, 842-847, July 14-17, 2019, Atlanta, USA.

1. **K. Qin,** B. J. Oommen. Chaotic Pattern Recognition Using the Adachi Neural Network Modified in a Small-World Way. In Proceedings of the *7th Chaotic Modeling and Simulation International Conference (Chaos2014)*, 391–398, Lisbon, Portugal, 2014

1. **K. Qin,** B. J. Oommen. Networking logistic neurons can yield chaotic and pattern recognition properties. In Proceedings of the *IEEE International Conference on Computational Intelligence for Measure Systems and Applications(ICMSA2011)*, 134–139, Ottawa, Canada, 2011.[ link](http://ieeexplore.ieee.org/abstract/document/6059914/)

1. **K. Qin,** B. J. Oommen. Chaotic and pattern recognition properties of a network of logistic neurons. In Proceedings of the *2nd International Conference on Computer Engineering and Technology (ICCET2010)*, vol.V3, 83–87, Chengdu, China, 2010.[ link](http://ieeexplore.ieee.org/abstract/document/5485779/)

1. **K. Qin,** M. T. Zhou, Y. Feng. A novel multicast key exchange algorithm based on extended chebyshev map. In Proceedings of the *4th International Conference on Complex, Intelligent and Software Intensive Systems (CISIS2010)*, 643–648, Kracow, Poland, 2010.[ link](http://ieeexplore.ieee.org/abstract/document/5447531/)

1. **K. Qin,** B. J. Oommen. Cryptanalysis of a Cryptographic Algorithm that Utilizes Chaotic Neural Networks. In Proceedings of the *29th International Symposium on Computer and Information Sciences (ISCIS2014)*, 167–174, Kracow, Poland, 2014.[ link](https://link.springer.com/chapter/10.1007/978-3-319-09465-6_18)

1. **K. Qin,** B. J. Oommen. Chaotic pattern recognition using the Adachi neural network modified in a random manner. In Proceedings of the *6th Chaotic Modeling and Simulation International Conference (Chaos2013)*., Istanbul, Turkey, 2013.

1. **K. Qin,** B. J. Oommen. Chaotic pattern recognition: The spectrum of properties of the Adachi neural network. In Proceedings of the *International Conference on Structural and Syntactic Pattern Recognition and Statistical Techniques in Pattern Recognition (SSSPR2008)*, Vol. 5342,540–550, Florida, USA, 2008.[ link](http://link.springer.com/chapter/10.1007/978-3-540-89689-0_58)

1. **K. Qin,** M. T. Zhou, N. Q. Liu, et al. A novel group key management based on Jacobian Elliptic Chebyshev Rational Map. In Proceedings of the *IFIP International Conference Network and Parallel Computing(NPC2007)*, 287–295, Dalian, China 2007.[ link](http://link.springer.com/chapter/10.1007/978-3-540-74784-0_30)

**已授权的专利（第一发明人）**

1. 一种基于上下文信息推理的生成式对话方法，专利号：	ZL 2021 1 0975993.4, 授权时间：2023.10.13, 授权公告号：CN 113656569 B

1. 一种基于深度学习的文本智能生成方法，专利号：ZL 2021 1 1331968.9， 授权时间：2023.05.12， 授权公告号：CN 113988274 B

1. 一种基于深度学习辅助艺术绘画的方法，专利号：ZL 2019 1 0629814.4， 授权时间：2023.04.18， 授权公告号：CN 110322529 B

1. 文本情感倾向的判别方法，专利号：ZL 2017 1 0812048.6， 授权时间：2020.11.03， 授权公告号：CN 107577665 B

1. 一种基于词频的skip语言模型的训练方法，专利号：ZL 2016 1 0522055.8， 授权时间：2019.03.15， 授权公告号：CN 106257441 B

1. 一种固定搭配型短语优先的两段式机器翻译方法，专利号： ZL 2016 1 0522056.2, 授权时间：2019.02.19， 授权公告号：CN 106156013 B

